<?php 


  function fetch_data () {
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");

    $conn = new mysqli("localhost", "root", "root", "web");

    $result = $conn->query("SELECT * FROM students");

    $outp = "";
    while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
      if ($outp != "") {$outp .= ",";}
      $outp .= '{"mis":"'  . $rs["mis"] . '",';
      $outp .= '"name":"'   . $rs["name"]  . '",';
      $outp .= '"year":"'. $rs["year"]  . '",';
      $outp .= '"fees":"'   . $rs["fees"]  . '",';
      $outp .= '"cgpa":"'   . $rs["cgpa"]  . '"}';
    }
    $outp ='{"records":['.$outp.']}';
    $conn -> close();
    echo($outp);
  }
    

  function delete_data () {
    $conn = new mysqli("localhost", "root", "root", "web");
    //decode json
    //delete query goes here
    $mis = $_GET['mis'];
    $result = $conn->query("DELETE FROM students WHERE mis= $mis");
    $conn -> close();
    
    fetch_data();
  }

  function update_data () {
    header("Access-Control-Allow-Origin: *");
    $conn = new mysqli("localhost", "root", "root", "web");
    //decode json
    //update query goes here
  
    $user = $_GET['user'];
    $user = json_decode($user, true);
    
    
    $mis=  $user['mis'];
    $name=  $user['name'];
    $year= $user['year'];
    $fees=  $user['fees'];
    $cgpa=  $user['cgpa'];
    //echo $cgpa;
    if($result = $conn->query("UPDATE students SET name = '$name', year = '$year', fees = '$fees', cgpa = '$cgpa' WHERE mis= '$mis' ")){
      
    }else {
      echo mysqli_error($conn);
    }
    
    //echo mysql_affected_rows();
    $conn -> close();
    fetch_data();
  }

  function add_data () {
    header("Access-Control-Allow-Origin: *");
    $conn = new mysqli("localhost", "root", "root", "web");
//decode json
    $newEntry = $_GET['user'];
    $newEntry = json_decode($newEntry, true);
    //add query goes here
    $mis = $newEntry['mis'];
    $name = $newEntry['name'];
    $year = $newEntry['year'];
    $fees = $newEntry['fees'];
    $cgpa = $newEntry['cgpa'];
    //echo $entry['mis'];
    $result = $conn->query("INSERT INTO students (`mis`, `name`, `year`, `fees`, `cgpa`) VALUES ('$mis', '$name', '$year', '$fees', '$cgpa')");
    $conn -> close();
    fetch_data();
  }

  function export_data () {
    
    header('Content-Type: text/csv; charset=utf-8');  
    header('Content-Disposition: attachment; filename=data.csv');  
    $output = fopen("../data.csv", "w"); 
    
    $conn = new mysqli("localhost", "root", "root", "web");
    fputcsv($output, array('MIS', 'Name', 'Year', 'Fees', 'CGPA'));  
    $query = "SELECT * from students ORDER BY mis";  
    
    $result = $conn->query($query);
    while($row = mysqli_fetch_assoc($result))  
    {  
         fputcsv($output, $row);  
    } 
    fclose($output);
    //echo $result;
    readfile("../data.csv");
  }

  if (isset($_GET['action'])) {
      if($_GET['action'] == "fetch_data") {
        fetch_data();
      }
      else if($_GET['action'] == "delete_data") {
        delete_data();
      }
      else if($_GET['action'] == "update_data") {
        update_data();
      }
      else if($_GET['action'] == "add_data") {
        add_data();
      }
      else if($_GET['action'] == 'export_data') {
        export_data();
      } 
    }
 ?>