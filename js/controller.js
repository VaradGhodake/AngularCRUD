app.controller("studentCntrl", function($scope, $http) {
  
  // $scope.data = {"records" : [{"mis":111503022, "name" : "Varad", "year" : 2015, "fees" : 32000, "cgpa" : 9.04},
  //                             {"mis":111503456, "name" : "Akshay", "year" : 2016, "fees" : 22000, "cgpa" : 8.67}],
  //           };
  // $scope.names = $scope.data.records;
  //$scope.user = {};
  $scope.names = [];
  $http.get("php/student.php", {params:{"action": "fetch_data"}})
  .then(function (response) {
      //console.log(response.data);
      $scope.names = response.data.records;
  });
  
  $scope.export = function() {
    $http.get("php/student.php", {params: {"action": "export_data"}})
    .then(function (response){
      console.log(response);
    });    
  };


  $scope.delete = function (index) {
  //  $scope.names.splice(index, 1);
  
    $http.get("php/student.php", {params : {"action": "delete_data", "mis": $scope.names[index]['mis']}})
    .then(function (response) {
        $scope.names = response.data.records;
//      console.log($scope.names[index]['mis']);
    });  
  };




  $scope.update = function (user) {
    //console.log(user);
    $http.get("php/student.php", {params:{"action": "update_data", "user": user}})
    .then(function (response) {
       alert("Record has been updated");
       $scope.names = response.data.records;
       //console.log(response.data);
    });

  };  

  $scope.add = function () {
    
    if(isNaN($scope.mis) || isNaN($scope.year) || isNaN($scope.fees) || isNaN($scope.cgpa) || $scope.cgpa > 10 || $scope.cgpa <0) {
      alert("Enter valid details")
      $scope.mis = null;
      $scope.name = null;
      $scope.year = null;
      $scope.fees = null;
      $scope.cgpa = null; 
      return;
    }
     var newEntry = {"mis" : $scope.mis, "name": $scope.name, "year" : $scope.year, "fees": $scope.fees, "cgpa": $scope.cgpa};
    // $scope.names.push(newEntry);

    console.log(newEntry);
    $http.get("php/student.php", {params:{"action": "add_data", "user": newEntry}})
    .then(function (response) {
        $scope.names = response.data.records;
    });    
    $scope.mis =$scope.name = $scope.year = $scope.fees =$scope.cgpa =  "" ;
  };  

});